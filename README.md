# Coding Challenge - Scrabble Stats 


## Setup

- You'll need a version 11 Java Development Kit (JDK) installed.  A good source is
[here](https://adoptopenjdk.net/?variant=openjdk11&jvmVariant=hotspot).

- Clone the repository: `git clone git@bitbucket.org:ghyston/java-scrabble-stats-exercise.git` and
create a new Git branch to do your work on.


- Open the project using IntelliJ (we'd recommend you install this via the [Jetbrains Toolbox
App](https://www.jetbrains.com/toolbox/app/))

- To run the application, either:

  - Select the 'ScrabbleStatsApplication' IntelliJ build configuration in the toolbar and use the
  green buttons to the right to either run or debug.
  
  - OR run `./mvnw spring-boot:run` from the command line in the project directory (omit the leading
  '`./`' if using cmd.exe)
  
  You should now have a Web API up and running that will listen to HTTP requests on port 8080 of your
computer, but how do we actually send HTTP requests to it?


### Postman

Postman is a tool we use to send requests to web APIs that we are developing so we can test them.

-   Download Postman from [here](https://www.getpostman.com/downloads/) and open it up
    
-   Click the cross in the top right on the first screen that comes up

First of all we are going to send a GET request to the API to retrieve some data back:

-   Where it says request URL you want to enter the URL of the endpoint that we want to hit with the
request, in the first case this will be `http://localhost:8080/api/scores`
    
-   Hit 'Send'

-   You should now see an empty JSON array (`[]`) was returned by the API:

![Postman GET response](./docs/images/postman-get.png)

Next we are going to POST some JSON to an endpoint:

-   Click on the '+' symbol on the request tab bar to create a new request

-   Click on the dropdown that says GET and change it to a POST
    
-   Enter the same URL as before, `http://localhost:8080/api/scores`
    
-   Under the URL bar click on 'body'
    
-   Select 'raw'

-   Change dropdown on same line from 'Text' to 'JSON (application/json)']
    
-   Add the following JSON:

    ```json
    {          
        "id": 5
    }          
    ```

-   Hit 'Send'

-   You should see the same JSON object returned by the API:

![Postman POST response](./docs/images/postman-post.png)

- Go back the to the 'GET' request tab and re-send the request to see the updated scores collection.


## The Task

Now that we have an example API running, and we can send requests to it, I want you to change the
API to track the results of games of Scrabble.

1.  First of all I would like an endpoint that I can POST the results of a game to using JSON. The
format of the JSON will be as follows:
    
    -   It will contain an array of players that took part in the game
    
    -   Each player will have an Id, their name and what their final score was

        ```json
        {
          "players": [
            {"id": 1, "name": "Ric", "score": 327},
            {"id": 2, "name": "Dani", "score": 72},
            {"id": 3, "name": "Bob", "score": 158} 
          ]
        }
        ```

2.  I would like an endpoint that I can send a GET request to that includes a player's Id and it
will return me a JSON object that contains some stats on that player. I will leave which stats get
returned up to you, but I would at least like to see:
    
    -   Games Played
    
    -   Average Points Per Game
    
    -   Games Won
    
3.  I would like an endpoint that I can send a GET request to that gives me stats about all the
games played, including:
    
    -   Total games played
    
    -   Name of player with most wins
    
    -   Name of player who has played the most games
    
    -   Name of player with highest average points per game

Initially we won't use a database to store the information that gets sent to the API (we will add
that in later). Instead you should store the data using a
[HashMap](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/HashMap.html) or
similar.
 
