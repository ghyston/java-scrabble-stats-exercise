package com.ghyston.training.scrabblestats.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ScoreNotFoundException extends RuntimeException {

    public ScoreNotFoundException(int id) {
        super("Could not find score with ID " + id);
    }
}
