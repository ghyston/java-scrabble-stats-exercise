package com.ghyston.training.scrabblestats.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Score {

    private final int id;

    @JsonCreator
    public Score(@JsonProperty(required = true) int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
