package com.ghyston.training.scrabblestats.controllers;

import com.ghyston.training.scrabblestats.models.Score;
import com.ghyston.training.scrabblestats.services.ScoresService;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path = "/api/scores")
@RestController
public class ScoresController {

    private final ScoresService scoresService;

    public ScoresController(ScoresService scoresService) {
        this.scoresService = scoresService;
    }

    @GetMapping
    public Iterable<Score> listScores() {
        return scoresService.getAllScores();
    }

    @GetMapping("{id}")
    public Score getScore(@PathVariable int id) {
        return scoresService.getScore(id);
    }


    @PostMapping
    public Score addScore(@RequestBody Score score) {
        scoresService.addScore(score);
        return score;
    }


    @PutMapping("{id}")
    public Score updateScore(@PathVariable int id, @RequestBody Score score) {
        if (score.getId() != id) {
            throw new IllegalArgumentException("IDs in URL and request body don't match");
        }
        // TODO
        throw new NotImplementedException("Update score not implemented");
    }


    @DeleteMapping("{id}")
    public void deleteScore(@PathVariable int id) {
        // TODO
        throw new NotImplementedException("Delete score not implemented");
    }
}
