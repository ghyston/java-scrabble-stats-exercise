package com.ghyston.training.scrabblestats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrabbleStatsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScrabbleStatsApplication.class, args);
    }

}
