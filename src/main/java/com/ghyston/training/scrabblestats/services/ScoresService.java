package com.ghyston.training.scrabblestats.services;

import com.ghyston.training.scrabblestats.exceptions.ScoreNotFoundException;
import com.ghyston.training.scrabblestats.models.Score;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ScoresService {

    private Map<Integer, Score> scoresById = new HashMap<>();

    public Iterable<Score> getAllScores() {
        return scoresById.values();
    }

    public void addScore(Score score) {
        scoresById.put(score.getId(), score);
    }

    public Score getScore(int id) {
        final Score score = scoresById.get(id);
        if (score == null) {
            throw new ScoreNotFoundException(id);
        }
        return score;
    }

}
