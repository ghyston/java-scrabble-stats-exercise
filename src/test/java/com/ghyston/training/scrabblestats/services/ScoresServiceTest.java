package com.ghyston.training.scrabblestats.services;

import com.ghyston.training.scrabblestats.models.Score;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

public class ScoresServiceTest {

    ScoresService scoresService = new ScoresService();

    @Test
    public void startsEmpty() {
        assertThat(scoresService.getAllScores()).isEmpty();
    }

    @Test
    public void afterAddingScore_containsSingleScore() {
        Score score = new Score(123);

        scoresService.addScore(score);

        assertThat(scoresService.getAllScores()).containsExactly(score);
    }

    @Test
    public void afterAddingScoresWithSameId_containsLatestScore() {
        Score score1 = new Score(123);
        Score score2 = new Score(123);
        assumeThat(score1).isNotEqualTo(score2);

        scoresService.addScore(score1);
        scoresService.addScore(score2);

        assertThat(scoresService.getAllScores()).containsExactly(score2);
    }

}